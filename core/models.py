# -*- coding: utf-8 -*-
from django.db import models
from datetime import datetime

import pytz

class Tweet(models.Model):
    class Meta:
        ordering = ('created_at', )

    id =  models.CharField(max_length=20, primary_key=True)
    created_at = models.DateTimeField()
    text =  models.TextField()
    user_id =  models.CharField(max_length=20)
    user_screen_name =  models.CharField(max_length=60)
    user_location =  models.CharField(max_length=60, default='')
    user_followers_count = models.IntegerField(default=0)
    user_friends_count = models.IntegerField(default=0)
    user_listed_count = models.IntegerField(default=0)
    user_created_at = models.DateTimeField()
    user_favourites_count = models.IntegerField(default=0)
    user_utc_offset = models.IntegerField(default=0)
    user_time_zone = models.CharField(max_length=20, default='')
    user_statuses_count = models.IntegerField(default=0)
    user_lang = models.CharField(max_length=4, default='')

    def days(self):
        return (datetime.now().replace(tzinfo=pytz.UTC) - self.user_created_at).days

    def reputation(self):
        try:
            value = float(self.user_friends_count)/(self.user_followers_count*(self.user_statuses_count/self.days()))
            return u'%.2f' % value
        except: return u'%.2f' % 0.0

    def __unicode__(self):
        return u'#%s' % self.id