# -*- coding: utf-8 -*-
from django.views.generic import TemplateView
from django.conf import settings

from models import Tweet


class IndexView(TemplateView):
    template_name = 'index.html'

    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)
        context['tweets'] = Tweet.objects.all()
        return context