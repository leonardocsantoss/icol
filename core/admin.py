# -*- coding: utf-8 -*-
from django.contrib import admin

from models import Tweet

class TweetAdmin(admin.ModelAdmin):
    list_display = ('id', 'created_at', 'user_screen_name', 'user_location', 'user_friends_count', 'user_followers_count', 'user_statuses_count', 'days', 'reputation')
    date_hierarchy = 'created_at'

admin.site.register(Tweet, TweetAdmin)