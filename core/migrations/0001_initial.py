# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Tweet',
            fields=[
                ('id', models.CharField(max_length=20, serialize=False, primary_key=True)),
                ('created_at', models.DateTimeField()),
                ('text', models.TextField()),
                ('user_id', models.CharField(max_length=20)),
                ('user_screen_name', models.CharField(max_length=60)),
                ('user_location', models.CharField(max_length=60)),
                ('user_followers_count', models.IntegerField()),
                ('user_friends_count', models.IntegerField()),
                ('user_listed_count', models.IntegerField()),
                ('user_created_at', models.DateTimeField()),
                ('user_favourites_count', models.IntegerField()),
                ('user_utc_offset', models.IntegerField()),
                ('user_time_zone', models.CharField(max_length=20)),
                ('user_statuses_count', models.IntegerField()),
                ('user_lang', models.CharField(max_length=4)),
            ],
        ),
    ]
