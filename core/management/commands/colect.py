#-*- coding: utf-8 -*-
from django.core.management.base import NoArgsCommand
from django.conf import settings

from core.models import Tweet
from twython import Twython

from datetime import datetime
from time import sleep
import pytz


class Command(NoArgsCommand):
    label = u'Rotina envio de emails'

    def handle_noargs(self, **options):

        twitter = Twython(settings.TWITTER_KEY, settings.TWITTER_SECRET, settings.TWITTER_ACCESS_TOKEN, settings.TWITTER_SECRET_TOKEN)
        for result in twitter.cursor(twitter.search, q='impeachment'):
            Tweet(
                id=result['id'],
                created_at=datetime.strptime(result['created_at'],'%a %b %d %H:%M:%S +0000 %Y').replace(tzinfo=pytz.UTC),
                text=result['text'],
                user_id=result['user']['id'],
                user_screen_name=result['user']['screen_name'],
                user_location=result['user']['location'],
                user_followers_count=result['user']['followers_count'],
                user_friends_count=result['user']['friends_count'],
                user_listed_count=result['user']['listed_count'],
                user_created_at=datetime.strptime(result['user']['created_at'], '%a %b %d %H:%M:%S +0000 %Y').replace(tzinfo=pytz.UTC),
                user_favourites_count=result['user']['favourites_count'],
                user_utc_offset=result['user']['utc_offset'] or 0,
                user_time_zone=result['user']['time_zone'] or '',
                user_statuses_count=result['user']['statuses_count'],
                user_lang=result['user']['lang'],
            ).save()
            sleep(5)